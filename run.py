from miniproject import app, models

if __name__ == '__main__':
    models.initialize()
    app.run(debug=True, host='0.0.0.0', port=9000)


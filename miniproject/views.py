from flask import render_template, request, redirect, url_for, Markup, flash
from miniproject import app, models
from lxml import etree

import json, tweepy, peewee, requests
# !-- Bad placement, ignore --! 
# Keys and secrets used for Twitter Api
CONSUMER_KEY = "1a8yl5rWHW53k6ZctFa8klhmp"
CONSUMER_SECRET = "KM3nF07sJRlc4bnkbr2uemGdHuuNgPe9jPBy7ZqPy0Q5ZnJ7d3"
ACCESS_TOKEN = "780706013552910336-JI336YzVJcGs2JXc8p423MoNa6cQdCw"
ACCESS_SECRET = "nYT4DWJtuUWZe0pThmisJtSuW4Q3wEZ1RRaVwvPwIn43K"

# "start"-page
@app.route('/')
@app.route('/index')
def index():
    user = {'nickname': 'Bo'} # temp fake user
    posts = [ # fake array of test posts
             {
                 'author': {'nickname': 'Andi'},
                 'body': 'Hello'
             },
             {
                 'author': {'nickname': 'Alexander'},
                 'body': 'World!'
             }
    ]

    return render_template('index.html',
                           user=user,
                          posts=posts)

# Page with forms for typing in your name
@app.route('/project1', methods = ['POST', 'GET'])
def typename():
    return render_template("form_submit.html")

# Page for when the submit button is hit
@app.route('/submitted', methods = ['POST'])
def postname():
    name1 = request.form['Name']

    models.update_username( 1, name1 )

	# Makes the flash twice? o.O
    flash('You have successfully updated the name in the database!')

    return redirect(url_for("typename"))

@app.route('/user', methods = [ 'GET' ])
def readname():

    id = request.args.get('Id')
    if(id != 1):
        id = 1

    user = models.get_user(id)

    return render_template("showuser.html", user = user)

@app.route('/project21', methods = ['GET', 'POST'])
def xmlCourses():

    return render_template('xmlcourses.html', data = transformXML())

@app.route('/project22', methods = ['GET', 'POST'])
def showTweets():

    return render_template('timeline.html', tweets = getTweets())

# Functions
def transformXML():
    source =\
    requests.get("http://cs.washington.edu/research/xmldatasets/data/courses/reed.xml").text

    source = etree.XML(source)

    with app.open_resource('static/xslcourse.xsl') as f:
        xslFile = f.read()

    transform = etree.XSLT(etree.XML(xslFile))
    source = transform(source)

    return Markup(source)

def getTweets():
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())

    timeline_data = api.user_timeline(screen_name="NiklasUlstrup", page=1)

    return timeline_data

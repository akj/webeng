<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:for-each select="root/course[position() &lt; 11]">
      <tr>
        <td><xsl:value-of select="reg_num"/></td>
        <td><xsl:value-of select="title"/></td>
        <td><xsl:value-of select="instructor"/></td>
      </tr>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>

from flask import Flask, render_template

from miniproject.resources.courses import courses_api
from miniproject.resources.reviews import reviews_api

app = Flask(__name__)
app.config.from_object('config')
app.secret_key = 'itsasecret'
app.register_blueprint(courses_api)
app.register_blueprint(reviews_api, url_prefix='/api/v1')

import miniproject.views

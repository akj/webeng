import datetime, sqlite3

from peewee import * 

# SQLite Manager addon til firefox
# https://addons.mozilla.org/da/firefox/addon/sqlite-manager/
DATABASE = SqliteDatabase('miniproject.sqlite')

class Users(Model):
    name = CharField()

    class Meta:
        database = DATABASE

class Course(Model):
    title = CharField()
    url = CharField(unique=True)
    created_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = DATABASE

class Review(Model):
    course = ForeignKeyField(Course, related_name='review_set')
    rating = IntegerField()
    comment = TextField(default='')
    created_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = DATABASE

def initialize():
    DATABASE.connect()
    DATABASE.create_tables([Users, Course, Review], safe=True)

def before_request_handler():
    database = DATABASE
    database.connect()

def after_request_handler():
    database = DATABASE
    database.close()

def get_user( id ):
    user = Users.get(Users.id==id)
    return user

def update_username( id, name ):
    user = Users.get(Users.id==id)
    user.name = name
    user.save()